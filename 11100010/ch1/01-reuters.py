from nltk.corpus import reuters

# check what is available in the corpus
files = reuters.fileids()
print(len(files))
print(files[:10])

# access the content of one of the files
words16097 = reuters.words(['test/16097'])
print(len(words16097))
print(words16097[:10])

# list the topics in the corpus
reutersGenres = reuters.categories()
print(len(reutersGenres))
print(reutersGenres[:10])

# access two topics and print out the words in a loosely sentenced 
# fashion as one sentence per line
for idx, w in enumerate(reuters.words(categories=['bop', 'cocoa'])):
    print(w+' ', end='')
    if w is '.':
        print()
    if idx == 10:
        break
