from nltk.corpus import CategorizedPlaintextCorpusReader
from random import randint

reader_pos = CategorizedPlaintextCorpusReader(r'tokens/pos', r'.*\.txt', cat_pattern=r'(\w+)/*')
#print(reader_pos.categories())
#print(reader_pos.fileids())

reader_neg = CategorizedPlaintextCorpusReader(r'tokens/neg', r'.*\.txt', cat_pattern=r'(\w+)/*')
#print(reader_neg.categories())
#print(reader_neg.fileids())

# choose a random article
fileP = reader_pos.fileids()[randint(0, len(reader_pos.fileids())-1)]
fileN = reader_neg.fileids()[randint(0, len(reader_neg.fileids())-1)]

print(fileP)
print(fileN)

for w in reader_pos.words(fileP):
    print(w + ' ', end='')
    if w is '.':
        print()

for w in reader_neg.words(fileN):
    print(w + ' ', end='')
    if w is '.':
        print()

"""
The quintessential ingredient of this recipe is the CategorizedPlaintextCorpusReader
class of NLTK. Since we already know that the corpus we have downloaded is categorized,
we only need provide appropriate arguments when creating the reader object. The
implementation of the CategorizedPlaintextCorpusReader class internally takes care
of loading the samples in appropriate buckets ('pos' and 'neg' in this case).
"""
