import nltk
from nltk.corpus import brown

print(brown.categories())

# w let's pick three genres--fiction, humor and romance--from this list as3.
# well as the whwords that we want to count out from the text of these three
# genres:

genres = ['fiction', 'humor', 'romance']
whwords = ['what', 'which', 'how', 'why', 'when', 'where', 'who']

"""
FreqDist() accepts a list of words and returns an object that contains the map
word and its respective frequency in the input word list. Here, the fdist object
will contain the frequency of each of the unique words in the genre_text word
list.
"""
for idx, _ in enumerate(genres):
    genre = genres[idx]
    print()
    print("Analysing '"+ genre + "' wh words")
    genre_text = brown.words(categories = genre)

    fdist = nltk.FreqDist(genre_text)

    for wh in whwords:
        print(wh + ':', fdist[wh], end=' ')

"""
We are iterating over the whwords word list, accessing the fdist object with each
of the wh words as index, getting back the frequency/count of all of them, and
printing them out.
After running the complete program, you will get this output:
['adventure', 'belles_lettres', 'editorial', 'fiction',
'government', 'hobbies', 'humor', 'learned', 'lore', 'mystery',
'news', 'religion', 'reviews', 'romance', 'science_fiction']
Analysing 'fiction' wh words
what: 128 which: 123 how: 54 why: 18 when: 133 where: 76 who: 103
Analysing 'humor' wh words
what: 36 which: 62 how: 18 why: 9 when: 52 where: 15 who: 48
Analysing 'romance' wh words
what: 121 which: 104 how: 60 why: 34 when: 126 where: 54 who: 89
How it works...
On analyzing the output, you can clearly see that we have the word count of all seven wh
words for the three picked genres on our console. By counting the population of wh words,
you can, to a degree, gauge whether the given text is high on relative clauses or question
sentences. Similarly, you may have a populated ontology list of important words that you
want to get a word count of to understand the relevance of the given text to your ontology.
Counting word populations and analyzing distributions of counts is one of the oldest,
simplest, and most popular tricks of the trade to start any kind of textual analysis.
"""
