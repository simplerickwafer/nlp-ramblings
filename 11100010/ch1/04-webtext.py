import nltk
from nltk.corpus import webtext

print(len(webtext.fileids()))
print(webtext.fileids())

fileid = 'singles.txt'
wbt_words = webtext.words(fileid)
fdist = nltk.FreqDist(wbt_words)

print('Count of the maximum appearing token "', fdist.max(), '" : ', fdist[fdist.max()])

print('Total Number of distinct tokens in the bag : ', fdist.N())

print('Following are the most common 10 words in the bag', fdist.most_common(10))

print('Frequency Distribution on Personal Advertisements')
print(fdist.tabulate())

#fdist.plot(cumulative=True)

"""
How it works...
Upon analyzing the output, we realize that all of it is very intuitive. But what is peculiar is
that most of it is not making sense. The token with maximum frequency count is ,. And
when you look at the 10 most common tokens, again you can't make out much about the
target dataset. The reason is that there is no preprocessing done on the corpus. In the third
chapter, we will learn one of the most fundamental preprocessing steps called stop words
treatment and will also see the difference it makes.
"""
