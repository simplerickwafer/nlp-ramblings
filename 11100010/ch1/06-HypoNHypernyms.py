from nltk.corpus import wordnet as wn

woman = wn.synset('woman.n.02')
bed = wn.synset('bed.n.01')

print(woman.hypernyms())
woman_paths = woman.hypernym_paths()

for idx, path in enumerate(woman_paths):
    print('\n\nHypernym Path :', idx + 1)
    for synset in path:
        print(synset.name(), ', ', end='')

types_of_beds = bed.hyponyms()

print('\n\nTypes of beds(Hyponyms): ', types_of_beds)

print(sorted(set(lemma.name() for synset in types_of_beds for lemma in synset.lemmas())))

"""
How it works...
As you can see, woman.n.01 has two hypernyms, namely adult and female, but it follows
four different routes in the hierarchy of WordNet database from the root node entity to
woman as shown in the output.
Similarly, the Synset bed.n.01 has 20 hyponyms; they are more specific and less
ambiguous (for nothing is unambiguous in English). Generally the hyponyms correspond
to leaf nodes or nodes very much closer to the leaves in the hierarchy as they are the least
ambiguous ones.
"""

