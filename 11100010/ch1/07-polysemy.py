"""
Polysemy means many possible meanings of a
word or a phrase. As we have already seen, English is an ambiguous language and more
than one meaning usually exists for most of the words in the hierarchy. Now, turning back
our attention to the problem statement, we must calculate the average polysemy based on
specific linguistic properties of all words in WordNet
"""
from nltk.corpus import wordnet as wn

stype = 'n'

synsets = wn.all_synsets(stype)

lemmas = []
for synset in synsets:
    for lemma in synset.lemmas():
        lemmas.append(lemma)

lemmas = set(lemmas)

count = 0
for lemma in lemmas:
    count = count + len(wn.synsets(lemma, stype))

print('Total distinct lemmas: ', len(lemmas))
print('Total senses :', count)
print('Average Polysemy of ', stype, ': ', count / len(lemmas))

"""
There is nothing much to say in this section, so I will instead give you some more
information on how to go about computing the polysemy of the rest of the types. As you
saw, Noun -> 'n'. Similarly, Verbs -> 'v', Adverbs -> 'r', and Adjective -> 'a' (hint # 3).
Now, I hope I have given you enough hints to get on with writing an NLP program of your
own and not be dependent on the feed of the recipes.
"""
