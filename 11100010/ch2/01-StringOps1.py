

namesList = ['Tuffy', 'Ali', 'Nysha', 'Tim']
sentence = 'My dog sleeps on sofa'

names = ';'.join(namesList)

print(type(names), ':', names)

wordList = sentence.split(' ')
print((type(wordList)), ':', wordList)

additionExample = 'ganesha' + 'ganesha' + 'ganesha'
multiplicationExample = 'ganesha' * 2
print('Text Additions :', additionExample)
print('Text Multiplication :', multiplicationExample)

strn = 'Python NLTK'
print(strn[1])
print(strn[-3])

