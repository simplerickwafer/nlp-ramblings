

strn = 'NLTK Dolly Python'
print('Substring starts from:', strn[11:])
print('Substring ends at:', strn[:4])
print('Substring :', strn[5:10])
print('Substring fancy:', strn[-12:-7])

if 'NLTK' in strn:
    print('found NLTK')

replaced = strn.replace('Dolly', 'Dorothy')
print('Replaced String:', replaced)

print('Accessing each character:')
for s in replaced:
    print(s)


