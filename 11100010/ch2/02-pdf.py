from PyPDF2 import PdfFileReader

def getTextPDF(pdfFileName, password = ''):

    pdf_file = open(pdfFileName, 'rb')
    read_pdf = PdfFileReader(pdf_file)

    if password != '':
        read_pdf.decrypt(password)

    #print(pdfFileName)
    #print(read_pdf.getNumPages())
    #print(read_pdf.getPage(0))
    #print(read_pdf.getPage(0).extractText())

    text = []
    for i in range(read_pdf.getNumPages()):
        text.append(read_pdf.getPage(i).extractText())

    pdf_file.close()

    return '\n'.join(text)

if __name__ == '__main__':

    pdfFile = 'sample-one-line.pdf'
    pdfFileEncrypted = 'sample-one-line.protected.pdf'
    
    print('PDF 1:\n', getTextPDF(pdfFile))
    print('PDF 2:\n', getTextPDF(pdfFileEncrypted, 'tuffy'))

"""
PyPDF2 is a pure Python library that we use to extract content from PDFs. The library has
many more functionalities to crop pages, superimpose images for digital signatures, create
new PDF files, and much more. However, your purpose as an NLP engineer or in any text
analytics task would only be to read the contents. In step 2, it's important to open the file in
backwards seek mode since the PyPDF2 module tries to read files from the end when
loading the file contents. Also, if any PDF file is password protected and you do not decrypt
it before accessing its contents, the Python interpreter will throw a PdfReadError.
"""
