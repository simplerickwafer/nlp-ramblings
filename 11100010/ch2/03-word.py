"""
"""

import docx

def getTextWord(wordFileName):

    doc = docx.Document(wordFileName)

    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)

    return '\n'.join(fullText)

if __name__ == '__main__':

    docName = 'sample-one-line.docx'
    print('Document in full :\n', getTextWord(docName))

    doc = docx.Document(docName)
    print('Number of paragraphs :', len(doc.paragraphs))
    print('Paragraph 2:', doc.paragraphs[1].text)
    print('Paragraph 2 style:', doc.paragraphs[1].style)

    print('Paragraph 1:', doc.paragraphs[0].text)
    print('Number of runs in paragraph 1:', len(doc.paragraphs[0].runs))
    for idx, run in enumerate(doc.paragraphs[0].runs):
        print('Run %s : %s' % (idx, run.text))

    print('is Run 0 underlined:', doc.paragraphs[0].runs[5].underline)
    print('is Run 2 bold:', doc.paragraphs[0].runs[1].bold)
    print('is Run 7 italic:', doc.paragraphs[0].runs[3].italic)

"""
How it works…
First, we wrote a function in the word.py file that will read any given DOCX file and
return to us the full contents in a string object. The preceding output text you see is fairly
self-explanatory though some things I would like to elaborate are Paragraph and Run lines.
The structure of a .docx document is represented by three data types in the python-docx
library. At the highest level is the Document object. Inside each document, we have multiple
paragraphs.

Every time we see a new line or a carriage return, it signifies the start of a new paragraph.
Every paragraph contains multiple Runs , which denotes a change in word styling. By
styling, we mean the possibilities of different fonts, sizes, colors, and other styling elements
such as bold, italic, underline, and so on. Each time any of these elements vary, a new run is
started.
"""
