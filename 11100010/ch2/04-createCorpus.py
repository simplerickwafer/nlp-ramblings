"""
How it works…
The output is fairly straightforward and as explained in the last step of the recipe. What is
peculiar is the characteristics of each of objects on show. The first line is the list of all words
in the new corpus; it doesn't have anything to do with higher level structures like
sentences/paragrpahs/files and so on. The second line is the list of all sentences in the file
1.txt, of which each sentence is a list of words inside each of the sentences. The third line
is a list of paragraphs, of which each paragraph object is in turn a list of sentences, of which
each sentence is in turn a list of words in that sentence, all from the file 0.txt. As you can
see, a lot of structure is maintained in paragraphs and sentences.
"""

import os
import docx
from PyPDF2 import PdfFileReader
from nltk.corpus.reader.plaintext import PlaintextCorpusReader

def getTextWord(wordFileName):

    doc = docx.Document(wordFileName)

    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)

    return '\n'.join(fullText)


def getTextPDF(pdfFileName, password = ''):

    pdf_file = open(pdfFileName, 'rb')
    read_pdf = PdfFileReader(pdf_file)

    if password != '':
        read_pdf.decrypt(password)

    text = []
    for i in range(read_pdf.getNumPages()):
        text.append(read_pdf.getPage(i).extractText())

    pdf_file.close()

    return '\n'.join(text)

def getText(txtFileName):
    fil = open(txtFileName, 'r')
    inmem = fil.read()
    fil.close()
    return inmem

if __name__ == '__main__':

    newCorpusDir = 'mycorpus/'
    if not os.path.isdir(newCorpusDir):
        os.mkdir(newCorpusDir)

    txt1 = getText('sample_feed.txt')
    txt2 = getTextPDF('sample-pdf.pdf')
    txt3 = getTextWord('sample-one-line.docx')
    files = [txt1, txt2, txt3]

    for idx, f in enumerate(files):
        with open("{}{}.txt".format(newCorpusDir, idx), 'w') as fout:
            fout.write(f)

    newCorpus = PlaintextCorpusReader(newCorpusDir, '.*')
    print(newCorpus.words())
    print(newCorpus.sents(newCorpus.fileids()[1]))
    print(newCorpus.paras(newCorpus.fileids()[0]))

"""
['Five', 'months', '.', 'That', "'", 's', 'how', ...]
[['A', 'generic', 'NLP'], ['(', 'Natural', 'Language', 'Processing', ')', 'toolset'], ...]
[[['Five', 'months', '.']], [['That', "'", 's', 'how', 'long', 'it', "'", 's', 'been', 'since', 'Mass', 'Effect', ':', 'Andromeda', 'launched', ',', 'and', 'that', "'", 's', 'how', 'long', 'it', 'took', 'BioWare', 'Montreal', 'to', 'admit', 'that', 'nothing', 'more', 'can', 'be', 'done', 'with', 'the', 'ailing', 'game', "'", 's', 'story', 'mode', '.'], ['Technically', ',', 'it', 'wasn', "'", 't', 'even', 'a', 'full', 'five', 'months', ',', 'as', 'Andromeda', 'launched', 'on', 'March', '21', '.']], ...]
"""
