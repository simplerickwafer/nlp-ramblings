"""
How it works…
Most of the RSS feeds you will get on the Internet will follow a chronological order, with the
latest post on top. Hence, the post we accessed in the recipe will be always be the most
recent post the feed is offering. The feed itself is ever-changing. So every time you run the
program, the format of the output will the remain same, but the content of the post on the
console may differ depending upon how fast the feed updates. Also, here we are directly
displaying the raw HTML on the console and not the clean content. Up next, we are going
to look at parsing HTML and getting only the information we need from a page. Again, a
further addendum to this recipe could be to read any feed of your choice, store all the posts
from the feed on disk, and create a plain text corpus using it. Needless to say, you can take
inspiration from the previous and the next recipes.
"""
import feedparser

myFeed = feedparser.parse("http://feeds.mashable.com/Mashable")

print('Feed Title :', myFeed['feed']['title'])
print('Number of posts :', len(myFeed.entries))

post = myFeed.entries[0]
print('Post Title :', post.title)

content = post.content[0].value
print('Raw content :\n', content)

"""
Feed Title : Mashable
Number of posts : 30
Post Title : Apple just surprised us with a limited sale on Beats by Dre headphones
Raw content :
 <img alt="" src="https://i.amz.mshcdn.com/rhgvf2WwpBaDgdIas6c2UH5HATc=/575x323/filters:quality(90)/https%3A%2F%2Fblueprint-api-production.s3.amazonaws.com%2Fuploads%2Fcard%2Fimage%2F781081%2Fc42631e5-3398-410e-8014-6a3a2d32c2e7.jpg...
...
"""
