"""
How it works…
BeautifulSoup 4 is a very handy library used to parse any HTML and XML content. It
supports Python's inbuilt HTML parser, but you can also use other third-party parsers with
it, for example, the lxml parser and the pure-Python html5lib parser. In this recipe, we
used the Python inbuilt HTML parser. The output generated is pretty much selfexplanatory,
and of course, the assumption is that you do know what HTML is and how to
write simple HTML.
"""

from bs4 import BeautifulSoup

html_doc = open('sample-html.html', 'r').read()
soup = BeautifulSoup(html_doc, 'html.parser')

print('\nFull text HTML Stripped:')
print(soup.get_text())

print('Accessing the <title> tag :', end=' ')
print(soup.title)

print('Accessing the text of <H1> tag :', end=' ')
print(soup.h1.string)

print('Accessing property of <img> tag :', end=' ')
print(soup.img['alt'])

print('\nAccessing all occurences of the <p> tag :')
for p in soup.find_all('p'):
    print(p.string)

