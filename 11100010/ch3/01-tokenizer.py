"""
How it works…
We saw three tokenizer classes and a method implemented to do the job in the NLTK
module. It's not very difficult to understand how to do it, but it is worth knowing why we
do it. The smallest unit to process in language processing task is a token. It is very much like
a divide-and-conquer strategy, where we try to make sense of the smallest units at a
granular level and add them up to understand the semantics of the sentence, paragraph,
document, and the corpus (if any) by moving up the level of detail.
"""

from nltk.tokenize import LineTokenizer, SpaceTokenizer, TweetTokenizer
from nltk import word_tokenize

lTokenizer = LineTokenizer()
text = "My name is Maximus Decimus Meridius, commander of the Armies of the North, General of the Felix Legions and loyal servant to the true emperor Marcus Aurelius. \nFather to a murdered son, husband to a murdered wife. \nAnd I will have my vengance, in this life or the next."
print("\nLine tokenizer output :", lTokenizer.tokenize(text))

rawText = "By 11 o'clock on Sunday, the doctor shall open the dispensary."
sTokenizer = SpaceTokenizer()
print("\nSpace tokenizer output :", sTokenizer.tokenize(rawText))

print("\nWord Tokenizer output :", word_tokenize(rawText))

tTokenizer = TweetTokenizer()
print("Tweet tokenizer output :", tTokenizer.tokenize("This is a coooool #dummysmiley :-) :-p <3"))

"""
Line tokenizer output : ['My name is Maximus Decimus Meridius, commander of the Armies of the North, General of the Felix Legions and loyal servant to the true emperor Marcus Aurelius. ', 'Father to a murdered son, husband to a murdered wife. ', 'And I will have my vengance, in this life or the next.']

Space tokenizer output : ['By', '11', "o'clock", 'on', 'Sunday,', 'the', 'doctor', 'shall', 'open', 'the', 'dispensary.']

Word Tokenizer output : ['By', '11', "o'clock", 'on', 'Sunday', ',', 'the', 'doctor', 'shall', 'open', 'the', 'dispensary', '.']

Tweet tokenizer output : ['This', 'is', 'a', 'coooool', '#dummysmiley', ':-)', ':-p', '<3']
"""
