"""
How it works…
For some language processing tasks, we ignore the form available in the input text and
work with the stems instead. For example, when you search on the Internet for cameras, the
result includes documents containing the word camera as well as cameras, and vice versa. In
hindsight though, both words are the same; the stem is camera.
Having said this, we can clearly see that this method is quite error prone, as the spellings
are quite meddled with after a stemmer is done reducing the words. At times, it might be
okay, but if you really want to understand the semantics, there is a lot of data loss here. For
this reason, we shall next see what is called lemmatization.
"""

from nltk import PorterStemmer, LancasterStemmer, word_tokenize

raw = "My name is Maximus Decimus Meridius, commander of the Armies of the North, General of the Felix Legions and loyal servant to the true emperor, Marcus Aurelius. Father to a murdered son, husband to a murdered wife. And I will have my vengeance, in this life or the next."

tokens = word_tokenize(raw)

porter = PorterStemmer()
pStems = [porter.stem(t) for t in tokens]
print(pStems)

lancaster = LancasterStemmer()
lStems = [lancaster.stem(t) for t in tokens]
print(lStems)
