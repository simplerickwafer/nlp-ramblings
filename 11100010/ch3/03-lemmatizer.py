"""
How it works…
WordNetLemmatizer removes affixes only if it can find the resulting word in the
dictionary. This makes the process of lemmatization slower than Stemming. Also, it
understands and treats capitalized words as special words; it doesn’t do any processing for
them and returns them as is. To work around this, you may want to convert your input
string to lowercase and then run lemmatization on it.
All said and done, lemmatization is still not perfect and will make mistakes. Check the
input string and the result of this recipe; it couldn't convert 'murdered' to 'murder'.
Similarly, it will handle the word 'women' correctly but can't handle 'men'.
"""
from nltk import word_tokenize, PorterStemmer, WordNetLemmatizer

raw = "My name is Maximus Decimus Meridius, commander of the Armies of the North, General of the Felix Legions and loyal servant to the true emperor, Marcus Aurelius. Father to a murdered son, husband to a murdered wife. And I will have my vengeance, in this life or the next."

tokens = word_tokenize(raw)

porter = PorterStemmer()
stems = [porter.stem(t) for t in tokens]
print(stems)

lemmatizer = WordNetLemmatizer()
lemmas = [lemmatizer.lemmatize(t) for t in tokens]
print(lemmas)
