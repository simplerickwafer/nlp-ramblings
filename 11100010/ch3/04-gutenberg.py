"""
How it works...
If you look carefully at the output, the most common 10 words in the unprocessed or plain
list of words won't make much sense. Whereas from the preprocessed bag of words, the
most common 10 words such as god, lord, and man give us a quick understanding that we
are dealing with a text related to faith or religion.
The foremost objective of this recipe is to introduce you to the concept of stopwords
treatment for text preprocessing techniques that you would most likely have to do before
running any complex analysis on your data. The NLTK stopwords corpus contains stopwords
for 11 languages. When you are trying to analyze the importance of keywords in any
text analytics application, treating the stopwords properly will take you a long way.
Frequency distribution will help you get the importance of words. Statistically speaking,
this distribution would ideally look like a bell curve if you plot it on a two-dimensional
plane of frequency and importance of words.
"""

import nltk
from nltk.corpus import gutenberg
print(gutenberg.fileids())

gb_words = gutenberg.words('bible-kjv.txt')
words_filtered = [e for e in gb_words if len(e) >= 3]

stopwords = nltk.corpus.stopwords.words('english')
words = [w for w in words_filtered if w.lower() not in stopwords]

fdistPlain = nltk.FreqDist(words)
fdist = nltk.FreqDist(gb_words)

print('Following are the most common 10 words in the bag')
print(fdist.most_common(10))

print('Following are the most common 10 words in the bag minust the stopwords')
print(fdistPlain.most_common(10))

