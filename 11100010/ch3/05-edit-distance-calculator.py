"""
I've already given you a brief on the algorithm; now let’s see how the matrix table gets
populated with the algorithm. See the attached table here:

You've already seen how we initialized the matrix. Then we filled up the matrix using the
formula in algorithm. The yellow trail you see is the significant numbers. After the first
iteration, you can see that the distance is moving in the direction of 1 consistently and the
final value that we return is denoted by the green background cell.

Now, the applications of the edit distance algorithm are multifold. First and foremost, it is
used in spell checkers and auto-suggestions in text editors, search engines, and many such
text-based applications. Since the cost of comparisons is equivalent to the product of the
length of the strings to be compared, it is sometimes impractical to apply it to compare large
texts.
"""
from nltk.metrics.distance import edit_distance

def my_edit_distance(str1, str2):
    m = len(str1) + 1
    n = len(str2) + 1

    table = {}
    for i in range(m): table[i, 0] = i
    for j in range(n): table[0, j] = j

    for i in range(1, m):
        for j in range(1, n):
            cost = 0 if str1[i - 1] == str2[j - 1] else 1
            table[i, j] = min(table[i, j - 1] + 1, table[i - 1, j] + 1, table[i - 1, j - 1] + cost)

    return table[i, j]

if __name__ == '__main__':

    print("Our Algorithm :", my_edit_distance("hand", "and"))
    print("NLTK Algorithm:", edit_distance("hand", "and"))
