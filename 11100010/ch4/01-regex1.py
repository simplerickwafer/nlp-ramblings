import re

def text_match(text, patterns):

    if re.search(patterns, text):
        return "Found a match!"
    else:
        return "Not matched!"

if __name__ == '__main__':

    print(text_match("ac", "ab?"))
    print(text_match("abc", "ab?"))
    print(text_match("abbc", "ab?"))

    print(text_match("ac", "ab*"))
    print(text_match("abc", "ab*"))
    print(text_match("abbc", "ab*"))

    print(text_match("ac", "ab+"))
    print(text_match("abc", "ab+"))
    print(text_match("abbc", "ab+"))

    print(text_match("abbc", "ab{2}"))
    print(text_match("aabbbc", "ab{3,5}?"))
