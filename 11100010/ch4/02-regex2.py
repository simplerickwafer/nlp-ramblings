import re

def text_match(text, patterns):

    if re.search(patterns, text):
        return "Found a match!"
    else:
        return "Not matched!"

if __name__ == '__main__':

    print("Pattern to test starts and ends with")
    print(text_match("abbc", "^a.*c$"))

    print("Begin with a word")
    print(text_match("Tuffy eats pie, Loki eats peas!", "^\w+"))

    print("Finding a word which contains character, not start or end of the word")
    print(text_match("Tuffy eats pie, Loki eats peas!", "\Bu\B"))
