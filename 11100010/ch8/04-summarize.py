from gensim.summarization import summarize
from bs4 import BeautifulSoup
import requests

urls = {
    'Daff: Unproven Unification of Suffix Trees and Redundancy':
    'http://scigen.csail.mit.edu/scicache/610/scimakelatex.21945.none.html',
    'CausticIslet: Exploration of Rasterization':
    'http://scigen.csail.mit.edu/scicache/790/scimakelatex.1499.none.html'
}

urls = {
    'Comparing the Producer-Consumer Problem and Simulated Annealing':
    'http://scigen.csail.mit.edu/scicache/610/scimakelatex.12352.html',
    'An Emulation of E-Business':
    'http://scigen.csail.mit.edu/scicache/790/scimakelatex.22061.html'
}

for key, url in urls.items():
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    data = soup.get_text()
    pos1 = data.find("1  Introduction") + len("1  Introduction")
    pos2 = data.find("5  Related Work")
    text = data[pos1:pos2].strip()
    print("PAPER URL: {}".format(url))
    print("TITLE: {}".format(key))
    print("GENERATED SUMMARY: {}".format(summarize(text)))
    print()
