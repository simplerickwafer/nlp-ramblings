from sklearn.datasets import fetch_20newsgroups

newsgroups_train = fetch_20newsgroups(subset='train')
newsgroups_test = fetch_20newsgroups(subset='test')

x_train = newsgroups_train.data
x_test = newsgroups_test.data
y_train = newsgroups_train.target
y_test = newsgroups_test.target

print("List of all 20 categories:")
print(newsgroups_train.target_names)
print()
print("Sample Email:")
print(x_train[0])
print("Sample Target Category:")
print(y_train[0])
print(newsgroups_train.target_names[y_train[0]])
