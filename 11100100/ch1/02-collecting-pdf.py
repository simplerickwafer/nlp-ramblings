#pip install PyPDF2
#
import PyPDF2
from PyPDF2 import PdfFileReader

#create pdf file object
pdf = open('file.pdf', 'rb')

#create pdf reader object
pdf_reader = PyPDF2.PdfFileReader(pdf)

#check number of pages
print(pdf_reader.numPages)

#create a page object
page = pdf_reader.getPage(0)

#finally extract text from the page
print(page.extractText())

#close the file
pdf.close()
