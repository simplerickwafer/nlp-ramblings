#install docx
#pip install docx

#import library
from docx import Document

#create a word file object
doc = open('file.docx', 'rb')

#creating word reader object
document = docx.Document(doc)

#create empty string and call this document
#this document var stores each paragraph in the word doc
#for loop for each paragrapg and append

docu = ""

for para in document.paragraphs:
    docu += para.text

#to see the output call docu
print(docu)
