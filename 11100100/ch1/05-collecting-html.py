#pip install bs4
import urllib.request as urllib2
from bs4 import BeautifulSoup

response = urllib2.urlopne('https://en.wikipedia.org/wiki/Natural_language_processing')
html_doc = response.read()

#parsing
soup = BeautifulSoup(html_doc, 'html.parser')

#format
strhtm = soup.prettify()

#print a few lines
print(strhtm[:1000])

#output

# extract tag value
print(soup.title)
print(soup.title.string)
print(soup.a.string)
print(soup.b.string)

#output

# extract all instances of a particular tag
for x in soup.find_all('a'):
    print(x.string)

#output

# extract all text of a tag
for x in soup.find_all('p'):
    print(x.text)

# output


