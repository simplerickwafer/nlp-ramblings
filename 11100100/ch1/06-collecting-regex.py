import re
import requests

#run the split query
re.split('\s+', 'I like this book')

# output

#extract emails
doc = "For more details please mail us at: xyz@abc.com, prq@mno.com"
addresses = re.findall(r'[\w\.-]+@[\w\.-]+', doc)
for address in addresses:
    print(address)

#output

# replace email ids
doc = "For more details please mail us at: xyz@abc.com"
new_email_address = re.sub(r'([\w\.-]+)@([\w\.-]+)', r'pqr@mno.com', doc)
print(new_email_address)

#output


# extract data from ebook
url = 'https://www.gutenberg.org/files/2638/2638-0.txt'

def get_book(url):
    raw = requests.get(url).text

    #discard metadata
    start = re.search(r"\*\*\* START OF THIS PROJECT GUTENBERG EBOOK .* \*\*\*", raw).end()
    stop = re.search(r"II", raw).start()

    #keep relevant text
    text = raw[start:stop]
    return text

#processing
def preprocess(sentence):
    return re.sub('[^A-Za-z0-9.]+', ' ', sentence).lower()

book = get_book(url)
processed_book = preprocess(book)
print(processed_book)

#output

# count number of times the appears in the book
print(len(re.findall(r'the', processed_book)))

#output

#replace i with I
processed_book = re.sub(r'\si\s', " I ", processed_book)
print(processed_book)

#find all occurance of text in the format "abc--xyz"
re.findall(r'[a-zA-Z0-9]*--[a-zA-Z0-9]*', book)

#output


