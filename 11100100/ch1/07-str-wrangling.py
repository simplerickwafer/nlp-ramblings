# Replacing content

string_v1 = "I am exploring NLP"

# char extraction
print(string_v1[0])

# output
"""
"""

#To extract exploring
print(String_v1[5:14])

#output
"""
exploring
"""

#
String_v2 = String_v1.replace("exploring", "learning")
print(String_v2)
#Output
"""
I am learning NLP
"""

# Concatenating two strings
s1 = "nlp"
s2 = "machine learning"
s3 = s1+s2
print(s3)
#output
"""
nlpmachine learning
"""

# Searching for a substring in a string
var="I am learning NLP"
f= "learn"
var.find(f)
#output
"""
5
"""






