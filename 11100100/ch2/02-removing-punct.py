import re
import string
import pandas as pd

text=['This is introduction to NLP',
    'It is likely to be useful, to people ',
    'Machine learning is the new electrcity',
    'There would be less hype around AI and more action going forward',
    'python is the best tool!',
    'R is good langauage',
    'I like this book',
    'I want more books like this']

#convert list to data frame
df = pd.DataFrame({'tweet':text})
print(df)
#output
"""
                                               tweet
0                        This is introduction to NLP
1              It is likely to be useful, to people
2             Machine learning is the new electrcity
3  There would be less hype around AI and more ac...
4                           python is the best tool!
5                                R is good langauage
6                                   I like this book
7                        I want more books like this
"""

s = "I. like. This book!"
s1 = re.sub(r'[^\w\s]',s)
print(s1)

#output
"""
'I like This book'
"""
Or:
df['tweet'] = df['tweet'].str.replace('[^\w\s]',"")
df['tweet']
#output
"""
0                          this is introduction to nlp
1                  it is likely to be useful to people
2               machine learning is the new electrcity
3    there would be less hype around ai and more ac...
4                              python is the best tool
5                                  r is good langauage
6                                     i like this book
7                          i want more books like this
"""

s = "I. like. This book!"
for c in string.punctuation:
    s = s.replace(c,"")

print(s)
#output
"""
'I like This book'
"""
