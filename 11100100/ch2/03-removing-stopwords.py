import pandas as pd
import nltk
from nltk.corpus import stopwords

text=['This is introduction to NLP',
    'It is likely to be useful, to people ',
    'Machine learning is the new electrcity',
    'There would be less hype around AI and more action going forward',
    'python is the best tool!',
    'R is good langauage',
    'I like this book',
    'I want more books like this']

#convert list to data frame
df = pd.DataFrame({'tweet':text})
print(df)
#output
"""
                                               tweet
0                        This is introduction to NLP
1              It is likely to be useful, to people
2             Machine learning is the new electrcity
3  There would be less hype around AI and more ac...
4                           python is the best tool!
5                                R is good langauage
6                                   I like this book
7                        I want more books like this
"""

#remove stop words
stop = stopwords.words('english')
df['tweet'] = df['tweet'].apply(lambda x: " ".join(x for x in x.split() if x not in stop))
print(df['tweet'])
