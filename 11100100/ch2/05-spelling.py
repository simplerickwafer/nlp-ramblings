
import pandas as pd
from autocorrect import spell
from textblob import TextBlob

text=['Introduction to NLP',
    'It is likely to be useful, to people ',
    'Machine learning is the new electrcity',
    'R is good langauage',
    'I like this book',
    'I want more books like this']

#convert list to data frame
df = pd.DataFrame({'tweet':text})
print(df)
#output
"""
"""
#Install textblob library
!pip install textblob

#import libraries and use 'correct' function
df['tweet'].apply(lambda x: str(TextBlob(x).correct()))
#output
"""
0                        Introduction to NLP
1      It is likely to be useful, to people
2    Machine learning is the new electricity
3                         R is good language
4                           I like this book
5                I want more books like this
"""

#You can also use autocorrect library as shown below
#install autocorrect
"""
!pip install autocorrect
"""
print(spell(u'mussage'))
print(spell(u'sirvice'))
#output
"""
'message'
'service'
"""
