
import pandas as pd
import nltk
from textblob import TextBlob

text=['This is introduction to NLP',
    'It is likely to be useful, to people ',
    'Machine learning is the new electrcity',
    'There would be less hype around AI and more action going forward',
    'python is the best tool!',
    'R is good langauage',
    'I like this book',
    'I want more books like this']

#convert list to data frame
df = pd.DataFrame({'tweet':text})
print(df)
#output
"""
                                               tweet
0                        This is introduction to NLP
1              It is likely to be useful, to people
2             Machine learning is the new electrcity
3  There would be less hype around AI and more ac...
4                           python is the best tool!
5                                R is good langauage
6                                   I like this book
7                        I want more books like this
"""
#Using textblob
TextBlob(df['tweet'][3]).words
#output
"""
WordList(['would', 'less', 'hype', 'around', 'ai', 'action', 'going', 'forward'])
"""

#using NLTK
#create data
mystring = "My favorite animal is cat"
nltk.word_tokenize(mystring)
#output
"""
['My', 'favorite', 'animal', 'is', 'cat']
"""

#using split function from python
mystring.split()
#output
['My', 'favorite', 'animal', 'is', 'cat']
