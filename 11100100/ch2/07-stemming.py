"""
Stemming is a process of extracting a root word.
"""
import pandas as pd
from nltk.stem import PorterStemmer

text=['I like fishing','I eat fish','There are many fishes in pound']

#convert list to dataframe
df = pd.DataFrame({'tweet':text})
print(df)

#output
"""
                            tweet
0                  I like fishing
1                      I eat fish
2  There are many fishes in pound
"""

st = PorterStemmer()
df['tweet'][:5].apply(lambda x: " ".join([st.stem(word) for word in x.split()]))
#output
"""
0                     I like fish
1                      I eat fish
2    there are mani fish in pound
"""


