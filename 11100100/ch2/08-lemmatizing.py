"""
Lemmatization is a process of extracting a root word by considering the vocabulary. For example, “good,” “better,” or “best” is lemmatized into good.
The part of speech of a word is determined in lemmatization.
It will return the dictionary form of a word, which must be a valid word while stemming just extracts the root word.
"""
import pandas as pd
from textblob import Word

text = ['I like fishing','I eat fish','There are many fishes in pound', 'leaves and leaf']

#convert list to dataframe
df = pd.DataFrame({'tweet':text})
print(df)
"""
                                tweet
0                      I like fishing
1                          I eat fish
2  There are multiple fishes in pound
3                     leaves and leaf
"""

#Import library
#Code for lemmatize
df['tweet'] = df['tweet'].apply(lambda x: " ".join([Word(word).lemmatize() for word in x.split()]))
print(df['tweet'])
#output
"""
0                      I like fishing
1                          I eat fish
2    There are multiple fish in pound
3                       leaf and leaf
"""
