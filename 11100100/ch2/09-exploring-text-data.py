"""
You want to explore and understand the text data.
"""

#Importing data
import string
import nltk
from nltk.corpus import webtext
from nltk.probability import FreqDist
from nltk.corpus import stopwords
from wordcloud import WordCloud
import matplotlib.pyplot as plt

#nltk.download('webtext')

wt_sentences = webtext.sents('firefox.txt')
wt_words = webtext.words('firefox.txt')

#Check number of words in the data Count the number of words:
len(wt_sentences)

#output
"""
1142
"""
len(wt_words)

#output
"""
102457
"""

frequency_dist = nltk.FreqDist(wt_words)
print(frequency_dist)

#showing only top few results
"""
FreqDist({'slowing': 1,
          'warnings': 6,
          'rule': 1,
          'Top': 2,
          'XBL': 12,
          'installation': 44,
          'Networking': 1,
          'inccorrect': 1,
          'killed': 3,
          ']"': 1,
          'LOCKS': 1,
          'limited': 2,
          'cookies': 57,
          'method': 12,
          'arbitrary': 2,
          'b': 3,
          'titlebar': 6,
"""
sorted_frequency_dist =sorted(frequency_dist,key=frequency_dist.__getitem__, reverse=True)
print(sorted_frequency_dist)

"""
['.',
'in',
'to',
'"',
'the',
"'",
'not',
'-',
'when',
'on',
'a',
'is',
't',
'and',
'of',
"""


#Consider words with length greater than 3 and plot
large_words = dict([(k,v) for k,v in frequency_dist.items() if len(k)>3])

frequency_dist = nltk.FreqDist(large_words)
frequency_dist.plot(50,cumulative=False)

#output
"""
image
"""

#install library
#!pip install wordcloud
#build wordcloud

wcloud = WordCloud().generate_from_frequencies(frequency_dist)

#plotting the wordcloud
plt.imshow(wcloud, interpolation='bilinear')
plt.axis("off")
"""
(-0.5, 399.5, 199.5, -0.5)
"""
plt.show()
#output
"""
image
"""
