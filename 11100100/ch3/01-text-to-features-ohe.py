"""
process of converting categorical variables
into features or columns and coding one or zero for the presence of that
particular category
"""

import pandas as pd

text = 'I am learning NLP'

# generate features
pd.get_dummies(text.split())
