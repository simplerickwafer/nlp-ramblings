"""
If a particular word
is appearing multiple times, there is a chance of missing the information
if it is not included in the analysis
"""

from sklearn.feature_extraction.text import CountVectorizer

# text
text = ["I love NLP and I will learn NLP in 2month "]

# create the transform
vectorizer = CountVectorizer()

# tokenize
vectorizer.fit(text)

# encode the doc
vector = vectorizer.transform(text)

#summarize and generate output
print(vectorizer.vocabulary_)
print(vector.toarray())

#output
# {'love': 4, 'nlp': 5, 'and': 1, 'will': 6, 'learn': 3, 'in': 2, '2month': 0}
# [[1 1 1 1 1 2 1]]

