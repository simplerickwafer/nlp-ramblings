"""
N-grams are the fusion of multiple letters or multiple words. They are
formed in such a way that even the previous and next words are captured.
• Unigrams are the unique words present in the sentence.
• Bigram is the combination of 2 words.
• Trigram is 3 words and so on
"""

from textblob import TextBlob

text = "I am learning NLP"

#For unigram : Use n = 1
print(TextBlob(Text).ngrams(1))

#output
"""
[WordList(['I']), WordList(['am']), WordList(['learning']), WordList(['NLP'])]
"""

#For Bigram : For bigrams, use n = 2
print(TextBlob(Text).ngrams(2))

#output
"""
[WordList(['I', 'am']), WordList(['am', 'learning']), WordList(['learning', 'NLP'])]
"""

#we will use count vectorizer to generate features.
#Using the same function, let us generate bigram features and see what the
#output looks like.

from sklearn.feature.extraction.text import CountVectorizer

# text
text = ["I love NLP and I will learn NLP in 2month "]

# create the transform
vectorizer = CountVectorizer(ngram_range(2,2))

# tokenize
vectorizer.fir(text)

# summarize and generate output
print(vectorizer.vocabulary_)
print(vector.toarray())

# output
"""
{'love nlp': 3, 'nlp and': 4, 'and will': 0, 'will learn': 6, 'learn nlp': 2, 'nlp in': 5, 'in 2month': 1}
[[1 1 1 1 1 1 1]]
"""
