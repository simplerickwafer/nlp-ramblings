"""
"""
#import library
#!pip install gensim
import gensim
from gensim.models import Word2Vec
from sklearn.decomposition import PCA
from matplotlib import pyplot

sentences = [['I', 'love', 'nlp'],
                  ['I', 'will', 'learn', 'nlp', 'in', '2','months'],
                  ['nlp', 'is', 'future'],
                  ['nlp', 'saves', 'time', 'and', 'solves', 'lot', 'of', 'industry', 'problems'],
                  ['nlp', 'uses', 'machine', 'learning']]

# training the model
skipgram = Word2Vec(sentences, size =50, window = 3, min_count=1, sg = 1)
print(skipgram)

# access vector for one word
print(skipgram['nlp'])

"""
[ 0.00552227 -0.00723104  0.00857073  0.00368054 -0.00071274
  0.00837146
  0.00179965 -0.0049786  -0.00448666 -0.00182289  0.00857488
-0.00499459
  0.00188365 -0.0093498   0.00174774 -0.00609793 -0.00533857
-0.007905
-0.00176814 -0.00024082 -0.00181886 -0.00093836 -0.00382601
-0.00986026
  0.00312014 -0.00821249  0.00787507 -0.00864689 -0.00686584
-0.00370761
  0.0056183   0.00859488 -0.00163146  0.00928791  0.00904601  
  0.00443816
-0.00192308  0.00941    -0.00202355 -0.00756564 -0.00105471
  0.00170084
  0.00606918 -0.00848301 -0.00543473  0.00747958  0.0003408
  0.00512787
-0.00909613  0.00683905]

Since our vector size parameter was 50, the model gives a vector of size
50 for each word
"""

# access vector for another one word
print(skipgram['deep'])
#KeyError: "word 'deep' not in vocabulary"

"""
We get an error saying the word doesn’t exist because this word was
not there in our input training data. This is the reason we need to train the
algorithm on as much data possible so that we do not miss out on words.
There is one more way to tackle this problem. Read Recipe 3-6 in this
chapter for the answer.
"""

# save model
skipgram.save('skipgram.bin')

# load model
skipgram = Word2Vec.load('skipgram.bin')

# T – SNE plot
# T – SNE plot is one of the ways to evaluate word embeddings. Let’s generate it and see how it looks.
X = skipgram[skipgram.wv.vocab]
pca = PCA(n_components=2)
result = pca.fit_transform(X)

# create a scatter plot of the projection
pyplot.scatter(result[:, 0], result[:, 1])
words = list(skipgram.wv.vocab)
for i, word in enumerate(words):
    pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
pyplot.show()
