"""
fastText is the improvised version of word2vec. word2vec basically
considers words to build the representation. But fastText takes each
character while computing the representation of the word.
"""

from gensim.models import FastText
from sklearn.decomposition import PCA
from matplotlib import pyplot

#Example sentences
sentences = [['I', 'love', 'nlp'],
                   ['I', 'will', 'learn', 'nlp', 'in', '2','months'],
                   ['nlp', 'is', 'future'],
                   ['nlp', 'saves', 'time', 'and', 'solves',
                    'lot', 'of', 'industry', 'problems'],
                   ['nlp', 'uses', 'machine', 'learning']]
fast = FastText(sentences,size=20, window=1, min_count=1, workers=5, min_n=1, max_n=2)

# vector for word nlp
print(fast['nlp'])

"""
[-0.00459182  0.00607472 -0.01119007  0.00555629 -0.00781679
-0.01376211
  0.00675235 -0.00840158 -0.00319737  0.00924599  0.00214165
-0.01063819
  0.01226836  0.00852781  0.01361119 -0.00257012  0.00819397
-0.00410289
-0.0053979  -0.01360016]
"""

# vector for word deep
print(fast['deep'])
"""
[ 0.00271002
-0.00640606
  0.00637542
-0.00230879
  0.02009759
-0.00957186
-0.00963234
-0.00242539 -0.00771885 -0.00396854  0.0114902
-0.01248098 -0.01207364  0.01400793 -0.00476079
-0.01952532  0.01558956 -0.01581665  0.00510567
-0.02059373]

This is the advantage of using fastText. The “deep” was not present in
training of word2vec and we did not get a vector for that word. But since
fastText is building on character level, even for the word that was not
there in training, it will provide results. You can see the vector for the word
“deep,” but it's not present in the input data.
"""

# load model
fast = Word2Vec.load('fast.bin')

# visualize
X = fast[fast.wv.vocab]
pca = PCA(n_components=2)
result = pca.fit_transform(X)

# create a scatter plot of the projection
pyplot.scatter(result[:, 0], result[:, 1])
words = list(fast.wv.vocab)
for i, word in enumerate(words):
    pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
pyplot.show()

"""
The figure above shows the embedding representation for fastText.
If you observe closely, the words “love” and “solve” are close together in
fastText but in your skip-gram and CBOW, “love” and “learn” are near to
each other. This is an effect of character-level embeddings.
"""
