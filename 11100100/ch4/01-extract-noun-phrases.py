"""
You want to extract a noun phrase.
Noun Phrase extraction is important when you want to analyze the “who” in a sentence.
"""
import nltk
from textblob import TextBlob

#Extract noun
blob = TextBlob("John is learning natural language processing")
for np in blob.noun_phrases:
    print(np)
"""
Output:
john
natural language processing
"""
