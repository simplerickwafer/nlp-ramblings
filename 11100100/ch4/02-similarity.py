"""
There are many similarity metrics like Euclidian,
cosine, Jaccard, etc. Applications of text similarity can be found in areas
like spelling correction and data deduplication.

Here are a few of the similarity measures:
Cosine similarity: Calculates the cosine of the angle between the two vectors.
Jaccard similarity: The score is calculated using the intersection or union of words.
Jaccard Index = (the number in both sets) / (the number in either set) * 100.
Levenshtein distance: Minimal number of insertions, deletions, and replacements required for transforming string “a” into string “b.”

Hamming distance: Number of positions with the same symbol in both strings. But it can be defined only for strings with equal length.
"""

#You want to find the similarity between texts/documents.

documents = (
"I like NLP",
"I am exploring NLP",
"I am a beginner in NLP",
"I want to learn NLP",
"I like advanced NLP"
)

#Import libraries
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

#Compute tfidf : feature engineering(refer previous chapter – Recipe 3-4)
tfidf_vectorizer = TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(documents)
print(tfidf_matrix.shape)
"""
#output
(5, 10)
"""

#compute similarity for first sentence with rest of the sentences
cosine_similarity(tfidf_matrix[0:1],tfidf_matrix)
"""
#output
array([[ 1.       ,  0.17682765,  0.14284054,  0.13489366,  0.68374784]])
If we clearly observe, the first sentence and last sentence have higher
similarity compared to the rest of the sentences.
"""
