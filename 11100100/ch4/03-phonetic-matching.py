"""
The next version of similarity checking is phonetic matching, which roughly
matches the two words or sentences and also creates an alphanumeric
string as an encoded version of the text or word. It is very useful for searching
large text corpora, correcting spelling errors, and matching relevant names.
Soundex and Metaphone are two main phonetic algorithms used for this
purpose. The simplest way to do this is by using the fuzzy library.
"""

import fuzzy

soundex = fuzzy.Soundex(4)

soundex('natural')
"""
#output
'N364'
"""

soundex('natuaral')
"""
#output
'N364'
"""

soundex('language')
"""
#output
'L52'
"""

soundex('processing')
"""
#output
'P625'
Soundex is treating “natural” and “natuaral” as the same, and the
phonetic code for both of the strings is “N364.” And for “language” and
“processing,” it is “L52” and “P625” respectively.
"""
