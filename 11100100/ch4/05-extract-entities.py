"""
In this recipe, we are going to discuss how to identify and extract entities
from the text, called Named Entity Recognition. There are multiple
libraries to perform this task like NLTK chunker, StanfordNER, SpaCy,
opennlp, and NeuroNER; and there are a lot of APIs also like WatsonNLU,
AlchemyAPI, NERD, Google Cloud NLP API, and many more.
Problem
You want to identify and extract entities from the text.
Solution
The simplest way to do this is by using the ne_chunk from NLTK or SpaCy
"""

sent = "John is studying at Stanford University in California"

import nltk
from nltk import ne_chunk
from nltk import word_tokenize

#NER
ne_chunk(nltk.pos_tag(word_tokenize(sent)), binary=False)

"""
#output
Tree('S', [Tree('PERSON', [('John', 'NNP')]), ('is', 'VBZ'),
('studying', 'VBG'), ('at', 'IN'), Tree('ORGANIZATION',
[('Stanford', 'NNP'), ('University', 'NNP')]), ('in', 'IN'),
Tree('GPE', [('California', 'NNP')])])
Here "John" is tagged as "PERSON"
"Stanford" as "ORGANIZATION"
"California" as "GPE". Geopolitical entity, i.e. countries, cities, states.
"""

#Using SpaCy
import spacy
nlp = spacy.load('en')

# Read/create a sentence
doc = nlp(u'Apple is ready to launch new phone worth $10000 in New york time square ')
for ent in doc.ents:
   print(ent.text, ent.start_char, ent.end_char, ent.label_)
"""
#output
Apple 0 5 ORG
10000 42 47 MONEY
New york 51 59 GPE

According to the output, Apple is an organization, 10000 is money, and New York is place.
"""
