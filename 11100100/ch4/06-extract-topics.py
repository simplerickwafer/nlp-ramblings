"""
Problem
You want to extract or identify topics from the document.
Solution
The simplest way to do this by using the gensim library.
How It Works
Let’s follow the steps in this section to identify topics within documents
using genism.
"""

doc1 = "I am learning NLP, it is very interesting and exciting.  it includes machine learning and deep learning"
doc2 = "My father is a data scientist and he is nlp expert"
doc3 = "My sister has good exposure into android development"

doc_complete = [doc1, doc2, doc3]

print(doc_complete)
"""
#output
['I am learning NLP, it is very interesting and exciting. it
includes machine learning and deep learning',
'My father is a data scientist and he is nlp expert',
'My sister has good exposure into android development']
"""

# Install and import libraries
#pip install gensim
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string

# Text preprocessing as discussed in chapter 2
stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()

def clean(doc):
    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = " ".join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())

    return normalized

doc_clean = [clean(doc).split() for doc in doc_complete]

print(doc_clean)

"""
#output
[['learning',
  'nlp',
  'interesting',
  'exciting',
  'includes',
  'machine',
  'learning',
  'deep',
  'learning'],
['father', 'data', 'scientist', 'nlp', 'expert'],
['sister', 'good', 'exposure', 'android', 'development']]
"""

# Importing gensim
import gensim
from gensim import corpora

# Creating the term dictionary of our corpus, where every unique term is assigned an index.
dictionary = corpora.Dictionary(doc_clean)

# Converting a list of documents (corpus) into Document-Term Matrix using dictionary prepared above.
doc_term_matrix = [dictionary.doc2bow(doc) for doc in doc_clean]
print(doc_term_matrix)
"""
#output
[[(0, 1), (1, 1), (2, 1), (3, 1), (4, 3), (5, 1), (6, 1)],
[(6, 1), (7, 1), (8, 1), (9, 1), (10, 1)],
[(11, 1), (12, 1), (13, 1), (14, 1), (15, 1)]]
"""

# Creating the object for LDA model using gensim library
Lda = gensim.models.ldamodel.LdaModel

# Running and Training LDA model on the document term matrix for 3 topics.
ldamodel = Lda(doc_term_matrix, num_topics=3, id2word =
dictionary, passes=50)

# Results
print(ldamodel.print_topics())
"""
#output
[(0, '0.063*"nlp" + 0.063*"father" + 0.063*"data" +
0.063*"scientist" + 0.063*"expert" + 0.063*"good" +
0.063*"exposure" + 0.063*"development" + 0.063*"android" +
0.063*"sister"'), (1, '0.232*"learning" + 0.093*"nlp" +
0.093*"deep" + 0.093*"includes" + 0.093*"interesting" +
0.093*"machine" + 0.093*"exciting" + 0.023*"scientist" +
0.023*"data" + 0.023*"father"'), (2, '0.087*"sister" +
0.087*"good" + 0.087*"exposure" + 0.087*"development" +
0.087*"android" + 0.087*"father" + 0.087*"scientist" +
0.087*"data" + 0.087*"expert" + 0.087*"nlp"')]

All the weights associated with the topics from the sentence seem
almost similar. You can perform this on huge data to extract significant
topics. The whole idea to implement this on sample data is to make you
familiar with it, and you can use the same code snippet to perform on the
huge data for significant results and insights.
"""

