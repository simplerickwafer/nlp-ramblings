"""
Problem
You want to do a sentiment analysis.
Solution
The simplest way to do this by using a TextBlob or vedar library.
How It Works
Let’s follow the steps in this section to do sentiment analysis using
TextBlob. It will basically give 2 metrics.
• Polarity = Polarity lies in the range of [-1,1] where 1
means a positive statement and -1 means a negative
statement.
• Subjectivity = Subjectivity refers that mostly it is a
public opinion and not factual information [0,1].
"""
review = "I like this phone. screen quality and camera clarity is really good."
review2 = "This tv is not good. Bad quality, no clarity, worst experience"

#import libraries
from textblob import TextBlob

#TextBlob has a pre trained sentiment prediction model
blob = TextBlob(review)
blob.sentiment
"""
#output
Sentiment(polarity=0.7, subjectivity=0.6000000000000001)
It seems like a very positive review.
"""
#now lets look at the sentiment of review2
blob = TextBlob(review2)
blob.sentiment
"""
#output
Sentiment(polarity=-0.6833333333333332,
subjectivity=0.7555555555555555)
This is a negative review, as the polarity is “-0.68.”
"""


