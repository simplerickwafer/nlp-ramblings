"""
In the above texts, the word “bank” has different meanings based on
the context of the sentence.

The Lesk algorithm is one of the best algorithms for word sense
disambiguation. Let’s see how to solve using the package pywsd and nltk.
"""

Text1 = 'I went to the bank to deposit my money'
Text2 = 'The river bank was full of dead fishes'

#Install pywsd
#pip install pywsd
#Import functions
from nltk.corpus import wordnet as wn
from nltk.stem import PorterStemmer
from itertools import chain
from pywsd.lesk import simple_lesk

# Sentences
bank_sents = ['I went to the bank to deposit my money', 'The river bank was full of dead fishes']

# calling the lesk function and printing results for both the sentences
print ("Context-1:", bank_sents[0])
answer = simple_lesk(bank_sents[0],'bank')
print ("Sense:", answer)
print ("Definition : ", answer.definition())
print ("Context-2:", bank_sents[1])
answer = simple_lesk(bank_sents[1],'bank','n')
print ("Sense:", answer)
print ("Definition : ", answer.definition())

"""
Context-1: I went to the bank to deposit my money
Sense: Synset('depository_financial_institution.n.01')
Definition :  a financial institution that accepts deposits and channels the money into lending activities
Context-2: The river bank was full of dead fishes
Sense: Synset('bank.n.01')
Definition :  sloping land (especially the slope beside a body of water)
