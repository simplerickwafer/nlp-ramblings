"""
Problem
You want to convert speech to text.
Solution
The simplest way to do this by using Speech Recognition and PyAudio.
How It Works
Let’s follow the steps in this section to implement speech to text.
!pip install SpeechRecognition
!pip install PyAudio
"""

import speech_recognition as sr
"""
Now after you run the below code snippet, whatever you say on the
microphone (using recognize_google function) gets converted into text.
"""

r=sr.Recognizer()
with sr.Microphone() as source:
    print("Please say something")
    audio = r.listen(source)
    print("Time over, thanks")

try:
    print("I think you said: "+r.recognize_google(audio));
except:
    pass;

"""
#output
Please say something
Time over, thanks
I think you said: I am learning natural language processing

This code works with the default language “English.” If you speak in
any other language, for example Hindi, the text is interpreted in the form of
English, like as below:
"""
#code snippet
r=sr.Recognizer()
with sr.Microphone() as source:
    print("Please say something")
    audio = r.listen(source)
    print("Time over, thanks")
try:
    print("I think you said: "+r.recognize_google(audio));
except:
    pass;
"""
#output
Please say something
Time over, thanks
I think you said: aapka naam kya hai

If you want the text in the spoken language, please run the below code
snippet. Where we have made the minor change is in the recognize_
google –language(‘hi-IN’, which means Hindi).
"""
#code snippet
r=sr.Recognizer()
with sr.Microphone() as source:
    print("Please say something")
    audio = r.listen(source)
    print("Time over, thanks")
try:
    print("I think you said: "+r.recognize_google(audio, language ='hi-IN'));
except sr.UnknownValueError:
    print("Google Speech Recognition could not understand audio")
except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".format(e))
except:
    pass
