"""
Converting text to speech is another useful NLP technique.
Problem
You want to convert text to speech.
Solution
The simplest way to do this by using the gTTs library.
How It Works
Let’s follow the steps in this section to implement text to speech.
"""

from gtts import gTTS

#chooses the language, English('en')
convert = gTTS(text='I like this NLP book', lang='en', slow=False)

# Saving the converted audio in a mp3 file named
myobj.save("audio.mp3")
"""
#output
Please play the audio.mp3 file saved in your local machine to hear the audio.
"""
