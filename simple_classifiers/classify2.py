import numpy as np
import sklearn.datasets
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline

examples = []
examples.append('lord of the rings is a fantasy')
examples.append('game of thrones is a fantasy')
examples.append('the hobbit is a fantasy book')
examples.append('example 3')
examples.append('example 3')

target = np.zeros((5,), dtype=np.int64)
target[0] = 0
target[1] = 0
target[2] = 0
target[3] = 1
target[4] = 1

names = ['fantasy','ignore','fresh']

dataset = sklearn.datasets.base.Bunch(data=examples, target=target, target_names=names)

print(dataset)

model = make_pipeline(TfidfVectorizer(), MultinomialNB())

model.fit(dataset.data, dataset.target)

#print(dataset)

def predict_category(s, train=dataset, model=model):
    pred = model.predict([s])
    return train.target_names[pred[0]]

print(predict_category('throne'))
print(predict_category('example'))

